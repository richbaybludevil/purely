Purely is a Tumblr theme under active development. It is based on Pure CSS framework and uses jQuery. It is responsive, feature rich yet light weight.

Everyone knows Tumblr has an extremely easy theme-ing tool (aka Customize Theme) and a super-useful blogging type (Text, Image, Set, Video, Audio, Chat and Quote). With all these features, Tumblr also has some limitations — the text post type has pure design elements; it uses almost same template for home, tag and search pages; reblogging feature dilutes the originality of blog; no default theme tag to display featured posts; and so many other.

Based on Pure CSS Framework with a minimal footprint. Pure is ridiculously tiny. The entire set of modules clocks in at 4.3KB minified and gzipped, without forgoing responsive styles, design, or ease of use. Crafted with mobile devices in mind, it was important to us to keep our file sizes small, and every line of CSS was carefully considered.

View.js enables Purely with an extremely light Lightbox plugin. And the lightbox looks similar to Tumblr’s default one. View.js displays higher resolution images on view.js lightbox. It is responsive and ultra-light. View.js only loads on the permalink page.

jQuery enables Purely to have great features, a Tumblr blog could provide — like Featured Posts, Feature Images and many more. jQuery is a light and reusable javascript library, millions of developers use.

Purely comes with Font Awesome icons. Font Awesome gives you scalable vector icons that can instantly be customized — size, color, drop shadow, and anything that can be done with the power of CSS. No need for icon images, just write <i class=”icon-plus”></i> to add a plus sign.



